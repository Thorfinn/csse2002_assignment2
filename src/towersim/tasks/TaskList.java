package towersim.tasks;

import java.util.List;

/**
 * Represents a circular list of tasks for an aircraft to cycle through.
 *
 * @ass1
 */
public class TaskList {
    /**
     * List of tasks to cycle through.
     */
    private final List<Task> tasks;
    /**
     * Index of current task in tasks list.
     */
    private int currentTaskIndex;

    /**
     * Creates a new TaskList with the given list of tasks.
     * <p>
     * Initially, the current task (as returned by {@link #getCurrentTask()}) should be the first
     * task in the given list.
     *
     * @param tasks list of tasks
     * @ass1
     */
    public TaskList(List<Task> tasks) {
        if (tasks == null || tasks.size() == 0) {
            throw new IllegalArgumentException("TaskList was null or empty");
        }
        // if list is of size 1, cannot compare task ordering validity, just allow it to create.
        if (!(tasks.size() == 1)) {
            // validate the passed in TaskList
            // check each Task 1 by 1 against it preceding task (cyclically)
            for (int i = 0; i < tasks.size(); i++) {
                // first task must check against the last task (cyclical).
                // all others can look to prior task
                if (i == 0) {
                    if (!validateTaskPair(tasks.get(0), tasks.get(tasks.size() - 1))) {
                        throw new IllegalArgumentException("TaskList had invalid Task ordering for"
                                + " tasks: " + tasks.get(0) + " and " + tasks.get(tasks.size() - 1)
                                + " at index positions: 0 and " + (tasks.size() - 1));
                    }
                } else {
                    if (!validateTaskPair(tasks.get(i), tasks.get(i - 1))) {
                        throw new IllegalArgumentException("TaskList had invalid Task ordering for"
                                + " tasks: " + tasks.get(i) + " and " + tasks.get(i - 1)
                                + " at index positions: " + i + " and " + (i - 1));
                    }
                }
            }
        }
        this.tasks = tasks;
        this.currentTaskIndex = 0;
    }

    /**
     * Validator Helper for TaskList. Compares Current and Prior Task.
     * Ensures that the
     * <p>
     * if current task is AWAY - Must have prior task of AWAY or TAKEOFF
     * if current task is LAND - Must have prior task of AWAY
     * if current task is WAIT - Must have prior task of LAND or WAIT
     * if current task is LOAD - Must have prior task of WAIT or LAND
     * if current task is TAKEOFF - Must have prior task of LOAD
     *
     * @param currentTask the current task
     * @param priorTask   the prior task
     * @return validate task pair
     */
    private boolean validateTaskPair(Task currentTask, Task priorTask) {
        if (TaskType.AWAY.equals(currentTask.getType())) {
            if (TaskType.AWAY.equals(priorTask.getType())
                    || TaskType.TAKEOFF.equals(priorTask.getType())) {
                return true;
            }
        }
        if (TaskType.LAND.equals(currentTask.getType())) {
            if (TaskType.AWAY.equals(priorTask.getType())) {
                return true;
            }
        }
        if (TaskType.WAIT.equals(currentTask.getType())) {
            if (TaskType.WAIT.equals(priorTask.getType())
                    || TaskType.LAND.equals(priorTask.getType())) {
                return true;
            }
        }
        if (TaskType.LOAD.equals(currentTask.getType())) {
            if (TaskType.WAIT.equals(priorTask.getType())
                    || TaskType.LAND.equals(priorTask.getType())) {
                return true;
            }
        }
        if (TaskType.TAKEOFF.equals(currentTask.getType())) {
            return TaskType.LOAD.equals(priorTask.getType());
        }

        // any other combination is invalid.
        return false;
    }

    /**
     * Returns the current task in the list.
     *
     * @return current task
     * @ass1
     */
    public Task getCurrentTask() {
        return this.tasks.get(this.currentTaskIndex);
    }

    /**
     * Returns the task in the list that comes after the current task.
     * <p>
     * After calling this method, the current task should still be the same as it was before calling
     * the method.
     * <p>
     * Note that the list is treated as circular, so if the current task is the last in the list,
     * this method should return the first element of the list.
     *
     * @return next task
     * @ass1
     */
    public Task getNextTask() {
        int nextTaskIndex = (this.currentTaskIndex + 1) % this.tasks.size();
        return this.tasks.get(nextTaskIndex);
    }

    /**
     * Moves the reference to the current task forward by one in the circular task list.
     * <p>
     * After calling this method, the current task should be the next task in the circular list
     * after the "old" current task.
     * <p>
     * Note that the list is treated as circular, so if the current task is the last in the list,
     * the new current task should be the first element of the list.
     *
     * @ass1
     */
    public void moveToNextTask() {
        this.currentTaskIndex = (this.currentTaskIndex + 1) % this.tasks.size();
    }

    /**
     * Returns the human-readable string representation of this task list.
     * <p>
     * The format of the string to return is
     * <pre>TaskList currently on currentTask [taskNum/totalNumTasks]</pre>
     * where {@code currentTask} is the {@code toString()} representation of the current task as
     * returned by {@link Task#toString()},
     * {@code taskNum} is the place the current task occurs in the task list, and
     * {@code totalNumTasks} is the number of tasks in the task list.
     * <p>
     * For example, a task list with the list of tasks {@code [AWAY, LAND, WAIT, LOAD, TAKEOFF]}
     * which is currently on the {@code WAIT} task would have a string representation of
     * {@code "TaskList currently on WAIT [3/5]"}.
     *
     * @return string representation of this task list
     * @ass1
     */
    @Override
    public String toString() {
        return String.format("TaskList currently on %s [%d/%d]",
                this.getCurrentTask(),
                this.currentTaskIndex + 1,
                this.tasks.size());
    }

    /**
     * Returns the machine-readable string representation of this task list.
     * The format of the string to return is
     * <p>
     * encodedTask1,encodedTask2,...,encodedTaskN
     * <p>
     * where encodedTaskX is the encoded representation of the Xth task in the task list,
     * for X between 1 and N inclusive, where N is the number of tasks in the task list
     * and encodedTask1 represents the current task.
     * <p>
     * For example, for a task list with 6 tasks and a current task of WAIT:
     * <p>
     * WAIT,LOAD@75,TAKEOFF,AWAY,AWAY,LAND
     *
     * @return encoded string representation of this task list
     */
    public String encode() {
        StringBuilder sb = new StringBuilder();

        // use indexing for loop to keep track of positions.
        for (int i = 0; i < tasks.size(); i++) {
            // need to make use of currentTaskIndex to ensure we populate TaskList
            // from the currentTask onwards.
            if (i + currentTaskIndex >= tasks.size()) {
                int circulatedBackIndex = i - tasks.size();
                sb.append(tasks.get(circulatedBackIndex + currentTaskIndex).encode()).append(",");
            } else {
                sb.append(tasks.get(i + currentTaskIndex).encode()).append(",");
            }
        }
        String taskListEncoded = sb.toString();
        // after connecting all the tasks and appending comma,
        // there will be 1 final comma to be removed on the end.
        // Note: only if there was at least 1 task.
        if (tasks.size() > 0) {
            taskListEncoded = taskListEncoded.substring(0, taskListEncoded.length() - 1);
        }
        return taskListEncoded;
    }
}
