package towersim.control;

import towersim.aircraft.Aircraft;
import towersim.aircraft.AircraftCharacteristics;
import towersim.aircraft.FreightAircraft;
import towersim.aircraft.PassengerAircraft;
import towersim.ground.AirplaneTerminal;
import towersim.ground.Gate;
import towersim.ground.HelicopterTerminal;
import towersim.ground.Terminal;
import towersim.tasks.Task;
import towersim.tasks.TaskList;
import towersim.tasks.TaskType;
import towersim.util.MalformedSaveException;
import towersim.util.NoSpaceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Utility class that contains static methods for loading a
 * control tower and associated entities from files.
 */
public class ControlTowerInitialiser {

    /**
     * holder for line reader between file read methods
     */
    private static String line = "";

    /**
     * holder for line reader between file read methods
     */
    private static int queueQuantity = 0;

    /**
     * holder for all Terminals created recursively in the bufferedReader in readTerminals
     */
    private static final List<Terminal> recursivelyPopulatedTerminals = new ArrayList<>();

    /**
     * <p>
     * Loads the number of ticks elapsed from the given reader instance.
     * The contents of the reader should match the format specified in the
     * tickWriter row of in the table shown in ViewModel.saveAs().
     *
     * <p>
     * For an example of valid tick reader contents, see the provided
     * saves/tick_basic.txt and saves/tick_default.txt files.
     *
     * <p>
     * The contents read from the reader are invalid if any of the following conditions are true:
     *
     * <p>
     * The number of ticks elapsed is not an integer
     * (i.e. cannot be parsed by Long.parseLong(String)).
     * The number of ticks elapsed is less than zero.
     *
     * @param reader reader from which to load the number of ticks elapsed
     * @return number of ticks elapsed
     * @throws MalformedSaveException if the format of the text read from
     *                                the reader is invalid according to the rules above
     * @throws IOException            if an IOException is encountered when reading from the reader
     */
    public static long loadTick(Reader reader) throws MalformedSaveException, IOException {
        // create a StringBuilder to append valid numbers to.
        StringBuilder sb = new StringBuilder();
        // read the first character from the file. This is ascii (eg '5' when read becomes '53')
        int readAsciiChar = reader.read();
        // -1 is returned when reached end of line.
        while (readAsciiChar != -1) {
            // use ascii table to only allow digits. (48 = 0, 49 = 1, ... 57 = 9)
            // this means newLines also fail.
            if (readAsciiChar < 48 || readAsciiChar > 57) {
                throw new MalformedSaveException("character read was not a valid positive number");
            }
            // convert to character representation of the read data.
            // results in the original int value, eg readAsciiChar = 53 -> becomes readNumber ='5'
            char readNumber = (char) readAsciiChar;
            sb.append(readNumber);

            // continue on to read next character in the line.
            readAsciiChar = reader.read();
        }

        try {
            long ticks = Long.parseLong(sb.toString());
            if (ticks < 0) {
                throw new MalformedSaveException("ticks was not positive");
            }
            return ticks;
        } catch (NumberFormatException e) {
            throw new MalformedSaveException("could not parse the result into a long value");
        }

    }

    /**
     * Loads the list of all aircraft managed by the control tower from the given reader instance.
     * The contents of the reader should match the format specified in the aircraftWriter row of
     * in the table shown in ViewModel.saveAs().
     *
     * <p>
     * For an example of valid aircraft reader contents, see the provided saves/aircraft_basic.txt
     * and saves/aircraft_default.txt files.
     *
     * <p>
     * The contents read from the reader are invalid if any of the following conditions are true:
     *
     * <p>
     * <ul>
     * <li>The number of aircraft specified on the first line of the reader is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The number of aircraft specified on the first line is not equal to the number of aircraft
     * actually read from the reader.
     * <li>Any of the conditions listed in the Javadoc for readAircraft(String) are true.
     * </ul>
     * This method should call readAircraft(String).
     *
     * @param reader reader from which to load the list of aircraft
     * @return list of aircraft read from the reader
     * @throws IOException            if an IOException is encountered when reading from the reader
     * @throws MalformedSaveException if the format of the text read from the reader is
     *                                invalid according to the rules above
     */
    public static List<Aircraft> loadAircraft(Reader reader)
            throws IOException, MalformedSaveException {
        List<Aircraft> aircrafts = new ArrayList<>();
        int aircraftNumber = 0;
        BufferedReader bufferedReader = new BufferedReader(reader);
        line = bufferedReader.readLine();
        boolean firstLine = true;
        while (line != null) {
            if (firstLine) {
                if (line.equals("0") || line.isEmpty()) {
                    return aircrafts;
                }
                aircraftNumber = Integer.parseInt(line);
                firstLine = false;
                line = bufferedReader.readLine();
                continue;
            }

            // for all the next rows, these should be Aircraft rows, try and call readAircraft
            // with the contents of this line
            Aircraft aircraft = readAircraft(line);
            aircrafts.add(aircraft);
            line = bufferedReader.readLine();

        }
        if (aircraftNumber != aircrafts.size()) {
            throw new MalformedSaveException("number of aircraft at top of file ("
                    + aircraftNumber + ") didn't match Aircraft List size ("
                    + aircrafts.size() + ")");
        }
        return aircrafts;
    }


    /**
     * Loads the takeoff queue, landing queue and map of loading aircraft from the
     * given reader instance.
     * Rather than returning a list of queues, this method does not return anything.
     * Instead, it should modify the given takeoff queue, landing queue and loading map
     * by adding aircraft, etc.
     *
     * <p>
     * The contents of the reader should match the format specified in the queuesWriter row of in
     * the table shown in ViewModel.saveAs().
     *
     * <p>
     * For an example of valid queues reader contents, see the provided saves/queues_basic.txt
     * and saves/queues_default.txt files.
     *
     * <p>
     * The contents read from the reader are invalid if any of the conditions listed in the
     * Javadoc for readQueue(BufferedReader, List, AircraftQueue) and
     * readLoadingAircraft(BufferedReader, List, Map) are true.
     *
     * <p>
     * This method should call readQueue(BufferedReader, List, AircraftQueue) and
     * readLoadingAircraft(BufferedReader, List, Map).
     *
     * @param reader          reader from which to load the queues and loading map
     * @param aircraft        list of all aircraft, used when validating that callsigns exist
     * @param takeoffQueue    empty takeoff queue that aircraft will be added to
     * @param landingQueue    empty landing queue that aircraft will be added to
     * @param loadingAircraft empty map that aircraft and loading times will be added to
     * @throws MalformedSaveException if the format of the text read from the reader
     *                                is invalid according to the rules above
     * @throws IOException            if an IOException is encountered when reading from the reader
     */
    public static void loadQueues(Reader reader, List<Aircraft> aircraft, TakeoffQueue
            takeoffQueue, LandingQueue landingQueue, Map<Aircraft, Integer> loadingAircraft)
            throws MalformedSaveException, IOException {

        BufferedReader bufferedReader = new BufferedReader(reader);
        line = bufferedReader.readLine();
        while (line != null) {
            if (line.equals("0") || line.isEmpty()) {
                break;
            }

            long count = line.chars().filter(ch -> ch == ':').count();
            if (1 != count) {
                throw new MalformedSaveException("incorrect number of colon "
                        + "separators found in line");
            }
            if (line.endsWith(":") || line.startsWith(":")) {
                throw new MalformedSaveException("Line started or ended with "
                        + "a data separator, ':'");
            }
            // split according to the colon separator.
            String[] takeoffQueueRowSplit = line.split(":");

            String queueName = takeoffQueueRowSplit[0];
            String aircraftQuantity = takeoffQueueRowSplit[1];

            try {
                queueQuantity = Integer.parseInt(aircraftQuantity);
            } catch (NumberFormatException n) {
                throw new MalformedSaveException("Aircraft Quantity " + aircraftQuantity
                        + " was not valid due to: " + n);
            }

            if (Integer.parseInt(aircraftQuantity) == 0) {
                // no aircraft in this queue. move on.
                line = bufferedReader.readLine();
                continue;
            }

            if (!(queueName.equals(LandingQueue.class.getSimpleName())
                    || queueName.equals("LoadingAircraft")
                    || queueName.equals(TakeoffQueue.class.getSimpleName()))) {
                throw new MalformedSaveException("Queue name invalid");
            }

            if (queueName.equals(TakeoffQueue.class.getSimpleName())) {
                readQueue(bufferedReader, aircraft, takeoffQueue);
            }
            if (queueName.equals(LandingQueue.class.getSimpleName())) {
                readQueue(bufferedReader, aircraft, landingQueue);
            }
            if (queueName.equals("LoadingAircraft")) {

                readLoadingAircraft(bufferedReader, aircraft, loadingAircraft);

            }
        }
    }

    /**
     * <p>
     * Loads the list of terminals and their gates from the given reader instance.
     * The contents of the reader should match the format specified in the
     * terminalsWithGatesWriter row of in the table shown in ViewModel.saveAs().
     *
     * <p>
     * For an example of valid queues reader contents, see the provided
     * saves/terminalsWithGates_basic.txt and saves/terminalsWithGates_default.txt files.
     *
     * <p>
     * The contents read from the reader are invalid if any of the following conditions are true:
     * <ul>
     *
     * <li>The number of terminals specified at the top of the file is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The number of terminals specified is not equal to the number of
     * terminals actually read from the reader.
     * <li>Any of the conditions listed in the Javadoc for readTerminal(String,
     * BufferedReader, List)
     * and readGate(String, List) are true.
     * </ul>
     *
     * <p>
     * This method should call readTerminal(String, BufferedReader, List).
     *
     * @param reader   from which to load the list of terminals and their gates
     * @param aircraft list of all aircraft, used when validating that callsigns exist
     * @return list of terminals (with their gates) read from the reader
     * @throws MalformedSaveException if the format of the text read from the
     *                                reader is invalid according to the rules above
     * @throws IOException            if an IOException is encountered when reading from the reader
     */
    public static List<Terminal> loadTerminalsWithGates(Reader reader, List<Aircraft> aircraft)
            throws MalformedSaveException, IOException {

        List<Terminal> terminals = new ArrayList<>();
        String strCurrentLine = "";
        Integer terminalCount = 0;
        boolean firstLine = true;
        BufferedReader bufferedReader = new BufferedReader(reader);
        while ((strCurrentLine = bufferedReader.readLine()) != null) {
            if (firstLine) {
                try {
                    terminalCount = Integer.parseInt(strCurrentLine);
                } catch (NumberFormatException e) {
                    throw new MalformedSaveException("Terminal Count was invalid due to" + e);
                }
                firstLine = false;
            }

            if (strCurrentLine.startsWith("AirplaneTerminal")
                    || strCurrentLine.startsWith("HelicopterTerminal")) {
                Terminal terminal = readTerminal(strCurrentLine, bufferedReader, aircraft);
                terminals = recursivelyPopulatedTerminals;
            }
        }

        if (terminals.size() != terminalCount) {
            throw new MalformedSaveException("Terminal count " + terminalCount
                    + " did not match list size " + terminals.size());
        }

        return terminals;
    }


    /**
     * <p>
     * Creates a control tower instance by reading various airport entities from the given readers.
     *
     * <p>
     * The following methods should be called in this order, and their results stored temporarily,
     * to load information from the readers:
     * <ul>
     * <li>loadTick(Reader) to load the number of elapsed ticks
     * <li>loadAircraft(Reader) to load the list of all aircraft
     * <li>loadTerminalsWithGates(Reader, List) to load the terminals and their gates
     * <li>loadQueues(Reader, List, TakeoffQueue, LandingQueue, Map) to load the takeoff queue,
     * landing queue and map of loading aircraft to their loading time remaining
     * </ul>
     *
     * <p>
     * Note: before calling loadQueues(), an empty takeoff queue and landing queue should be
     * created by calling their respective constructors.
     * Additionally, an empty map should be created by calling:
     * <p>
     * new TreeMap<>(Comparator.comparing(Aircraft::getCallsign))
     *
     * <p>
     * This is important as it will ensure that the map is ordered by aircraft
     * callsign (lexicographically).
     *
     * <p>
     * Once all information has been read from the readers, a new control tower should be
     * initialised
     * by calling ControlTower(long, List, LandingQueue, TakeoffQueue, Map). Finally, the terminals
     * that have been read should be added to the control tower by calling
     * ControlTower.addTerminal(Terminal).
     *
     * @param tick               reader from which to load the number of ticks elapsed
     * @param aircraft           reader from which to load the list of aircraft
     * @param queues             reader from which to load the aircraft queues and map of
     *                           loading aircraft
     * @param terminalsWithGates reader from which to load the terminals and their gates
     * @return control tower created by reading from the given readers
     * @throws MalformedSaveException if reading from any of the given readers results in a
     *                                MalformedSaveException, indicating the contents of that
     *                                reader are invalid
     * @throws IOException            if an IOException is encountered when reading from any
     *                                of the readers
     */
    public static ControlTower createControlTower(Reader tick, Reader aircraft, Reader queues,
                                                  Reader terminalsWithGates)
            throws MalformedSaveException, IOException {
        long ticks = loadTick(tick);
        List<Aircraft> aircrafts = loadAircraft(aircraft);
        List<Terminal> terminals = loadTerminalsWithGates(terminalsWithGates, aircrafts);
        LandingQueue landingQueue = new LandingQueue();
        TakeoffQueue takeoffQueue = new TakeoffQueue();

        Map<Aircraft, Integer> loadingMap = new TreeMap<>(
                Comparator.comparing(Aircraft::getCallsign));

        loadQueues(queues, aircrafts, takeoffQueue, landingQueue, loadingMap);

        ControlTower controlTower = new ControlTower(ticks, aircrafts, landingQueue,
                takeoffQueue, loadingMap);
        for (Terminal terminal : terminals) {
            controlTower.addTerminal(terminal);
        }

        return controlTower;
    }

    /**
     * <p>
     * Reads an aircraft from its encoded representation in the given string.
     *
     * <p>
     * If the AircraftCharacteristics.passengerCapacity of the encoded aircraft is greater
     * than zero,
     * then a PassengerAircraft should be created and returned. Otherwise, a FreightAircraft
     * should be created and returned.
     *
     * <p>
     * The format of the string should match the encoded representation of an aircraft,
     * as described in Aircraft.encode().
     *
     * <p>
     * The encoded string is invalid if any of the following conditions are true:
     * <ul>
     * <li>More/fewer colons (:) are detected in the string than expected.
     * <li>The aircraft's AircraftCharacteristics is not valid, i.e. it is not one of those
     * listed in AircraftCharacteristics.values().
     * <li>The aircraft's fuel amount is not a double (i.e. cannot be parsed by
     * Double.parseDouble(String)).
     * <li>The aircraft's fuel amount is less than zero or greater than the aircraft's
     * maximum fuel capacity.
     * <li>The amount of cargo (freight/passengers) onboard the aircraft is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The amount of cargo (freight/passengers) onboard the aircraft is less than zero
     * or greater than the aircraft's maximum freight/passenger capacity.
     * <li>Any of the conditions listed in the Javadoc for readTaskList(String) are true.
     * </ul>
     *
     * <p>
     * This method should call readTaskList(String).
     *
     * @param line line of text containing the encoded aircraft
     * @return decoded aircraft instance
     * @throws MalformedSaveException - if the format of the given string is invalid according
     *                                to the rules above
     */
    public static Aircraft readAircraft(String line) throws MalformedSaveException {
        // check there is exactly 5 colons ":" in the line
        long count = line.chars().filter(ch -> ch == ':').count();
        if (5 != count) {
            throw new MalformedSaveException("incorrect number of colon "
                    + "separators found in line");
        }
        if (line.endsWith(":") || line.startsWith(":")) {
            throw new MalformedSaveException("Line started or ended with "
                    + "a data separator, ':'");
        }
        if (line.contains("::")) {
            throw new MalformedSaveException("Line was missing data, found a double "
                    + "data separator, '::'");
        }

        // if so, split according to the colon separator.
        String[] aircraftRowSplit = line.split(":");

        // go through a build up the required parts to make an Aircraft if valid.
        // Callsign
        String callSignRead = aircraftRowSplit[0];
        if (callSignRead == null || callSignRead.isBlank())  {
            throw new MalformedSaveException("Invalid callSignRead");
        }

        // verify the Aircraft Characteristics matches enum, and set if ok.
        String aircraftCharacteristicsRead = aircraftRowSplit[1];
        AircraftCharacteristics aircraftCharacteristics = null;
        try {
            aircraftCharacteristics =
                    AircraftCharacteristics.valueOf(aircraftCharacteristicsRead);
        } catch (IllegalArgumentException e) {
            throw new MalformedSaveException("Invalid Aircraft Characteristic - " + e.getMessage());
        }

        // use taskList validator method to validate and create the Tasks.
        String tasksRead = aircraftRowSplit[2];
        TaskList tasks = readTaskList(tasksRead);

        // validate the fuel amount, check it is a valid double number.
        String fuelAmountRead = aircraftRowSplit[3];
        double fuelAmount = 0.0;
        try {
            fuelAmount = Double.parseDouble(fuelAmountRead);
        } catch (NumberFormatException n) {
            throw new MalformedSaveException("Fuel amount was invalid due to" + n.getMessage());
        }
        // also check its not < 0 or > than maximum allowed amount.
        if (fuelAmount < 0 || fuelAmount > aircraftCharacteristics.fuelCapacity) {
            throw new MalformedSaveException("Fuel amount of " + fuelAmount
                    + " was invalid due to being less than zero or greater than maximum. "
                    + " The maximum allowed capacity is " + aircraftCharacteristics.fuelCapacity);
        }


        // validate freight / passengers to create respective Aircraft Type.
        String freightOrPassengersRead = aircraftRowSplit[5];
        int freightOrPassengers = 0;
        try {
            freightOrPassengers = Integer.parseInt(freightOrPassengersRead);
        } catch (NumberFormatException n) {
            throw new MalformedSaveException("Could not parse the Freight or Passengers" + n);
        }
        Aircraft aircraft = null;
        if (aircraftCharacteristics.passengerCapacity > 0) {
            if (freightOrPassengers > aircraftCharacteristics.passengerCapacity
                    || freightOrPassengers < 0) {
                throw new MalformedSaveException("Invalid quantity of Passengers: "
                        + freightOrPassengers + "maximum allowed passengers is: "
                        + aircraftCharacteristics.passengerCapacity);
            }
            aircraft = new PassengerAircraft(callSignRead,
                    aircraftCharacteristics, tasks, fuelAmount, freightOrPassengers);

        }
        if (aircraftCharacteristics.freightCapacity > 0) {
            if (freightOrPassengers > aircraftCharacteristics.freightCapacity
                    || freightOrPassengers < 0) {
                throw new MalformedSaveException("Invalid amount of Freight: "
                        + freightOrPassengers + " - maximum allowed freight is: "
                        + aircraftCharacteristics.freightCapacity);
            }

            aircraft = new FreightAircraft(callSignRead,
                    aircraftCharacteristics, tasks, fuelAmount, freightOrPassengers);

        }

        // if neither fright or passenger capacity was > 0 and so Aircraft was not made
        // throw exception.
        if (aircraft == null) {
            throw new MalformedSaveException("Neither the Passenger or Freight capacity "
                    + "was > 0 and so no Aircraft was able to be created.");
        }


        String hasEmergencyRead = aircraftRowSplit[4];
        if (hasEmergencyRead.equals("true")) {
            aircraft.declareEmergency();
        }
        if (!(hasEmergencyRead.equals("false") || hasEmergencyRead.equals("true"))) {
            throw new MalformedSaveException("emergency status was '" + hasEmergencyRead
                    + "' which is neither 'true or 'false'");
        }

        return aircraft;
    }

    /**
     * <p>
     * Reads a task list from its encoded representation in the given string.
     *
     * <p>
     * The format of the string should match the encoded representation of a
     * task list, as described in TaskList.encode().
     *
     * <p>
     * The encoded string is invalid if any of the following conditions are true:
     * <ul>
     * <li>The task list's TaskType is not valid (i.e. it is not one of those listed in
     * TaskType.values()).
     * <li>A task's load percentage is not an integer (i.e. cannot be parsed by
     * Integer.parseInt(String)).
     * <li>A task's load percentage is less than zero.
     * <li>More than one at-symbol (@) is detected for any task in the task list.
     * <li>The task list is invalid according to the rules specified in TaskList(List).
     * </ul>
     *
     * @param taskListPart string containing the encoded task list
     * @return decoded task list instance
     * @throws MalformedSaveException if the format of the given string is invalid according
     *                                to the rules above
     */

    public static TaskList readTaskList(String taskListPart) throws MalformedSaveException {
        if (taskListPart == null || taskListPart.equals("")) {
            throw new MalformedSaveException("TaskList was null or empty when read from file.");
        }
        String[] tasksSplit = taskListPart.split(",");

        List<Task> tasksToBeDone = new ArrayList<>();
        for (String task : tasksSplit) {
            if (task.equals("LOAD")) {
                throw new MalformedSaveException("LOAD task missing @ and percent complete");
            }
            if (task.startsWith("LOAD")) {
                if (!task.contains("@")) {
                    throw new MalformedSaveException("LOAD task missing @ Symbol");
                }
            }
            if (task.startsWith("LOAD") && task.contains("@")) {
                // check there is exactly 1 At symbol "@" in the task
                long count = task.chars().filter(ch -> ch == '@').count();
                if (1 != count) {
                    throw new MalformedSaveException("incorrect number of '@' separators found "
                            + " in the Task");
                }
                String[] loadTaskSplit = task.split("@");
                TaskType loadTaskType = null;
                int loadPercent = -1;
                try {
                    loadTaskType = TaskType.valueOf(loadTaskSplit[0]);
                } catch (IllegalArgumentException e) {
                    throw new MalformedSaveException("Invalid Load Task Type found - " + e);
                }
                // loadPercent SHOULD be be the 2nd part after the @ symbol.
                try {
                    loadPercent = Integer.parseInt(loadTaskSplit[1]);
                } catch (IllegalArgumentException e) {
                    throw new MalformedSaveException("Invalid Load Task Percent found - " + e);
                }
                if (loadPercent < 0 || loadPercent > 100) {
                    throw new MalformedSaveException("Load percent was less than zero or"
                            + " greater than 100");
                }
                Task loadTask = new Task(loadTaskType, loadPercent);
                tasksToBeDone.add(loadTask);
            } else {
                try {
                    TaskType taskType = TaskType.valueOf(task);
                    Task otherTask = new Task(taskType);
                    tasksToBeDone.add(otherTask);
                } catch (IllegalArgumentException e) {
                    throw new MalformedSaveException("Invalid Task Type found - " + e);
                }

            }
        }
        try {
            return new TaskList(tasksToBeDone);
        } catch (IllegalArgumentException e) {
            throw new MalformedSaveException("Could not construct the task list due to: " + e);
        }

    }


    /**
     * <p>
     * Reads an aircraft queue from the given reader instance.
     *
     * <p>
     * Rather than returning a queue, this method does not return anything. Instead,
     * it should modify the given aircraft queue by adding aircraft to it.
     *
     * <p>
     * The contents of the text read from the reader should match the encoded representation
     * of an aircraft queue, as described in AircraftQueue.encode().
     *
     * <p>
     * The contents read from the reader are invalid if any of the following conditions are true:
     * <ul>
     * <li>The first line read from the reader is null.
     * <li>The first line contains more/fewer colons (:) than expected.
     * <li>The queue type specified in the first line is not equal to the simple
     * class name of the queue provided as a parameter.
     * <li>The number of aircraft specified on the first line is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The number of aircraft specified is greater than zero and the second
     * line read is null.
     * <li>The number of callsigns listed on the second line is not equal to the
     * number of aircraft specified on the first line.
     * <li>A callsign listed on the second line does not correspond to the callsign
     * of any aircraft contained in the list of aircraft given as a parameter.
     * </ul>
     *
     * @param reader   reader from which to load the aircraft queue
     * @param aircraft list of all aircraft, used when validating that callsigns exist
     * @param queue    empty queue that aircraft will be added to
     * @throws IOException            if the format of the text read from the reader is
     *                                invalid according to the rules above
     * @throws MalformedSaveException if an IOException is encountered when reading from the reader
     */

    public static void readQueue(BufferedReader reader, List<Aircraft> aircraft,
                                 AircraftQueue queue) throws IOException, MalformedSaveException {
        // read next line
        String possibleCallsignLine;
        while ((possibleCallsignLine = reader.readLine()) != null) {
            if (possibleCallsignLine == null || possibleCallsignLine.isEmpty()) {
                throw new MalformedSaveException("Invalid row read, was empty");
            }
            if (possibleCallsignLine.startsWith(LandingQueue.class.getSimpleName())
                    || possibleCallsignLine.startsWith("LoadingAircraft")
                    || possibleCallsignLine.startsWith(TakeoffQueue.class.getSimpleName())) {
                // completed this queue, move to next one.
                line = possibleCallsignLine; // this is a new queue
                return;
            }
            // should be on a callsign line, should be a comma separated list
            String[] callsigns = possibleCallsignLine.split(",");
            boolean aircraftAddedToQueue = false;
            for (String callsign : callsigns) {
                for (Aircraft a : aircraft) {
                    if (a.getCallsign().equals(callsign)) {
                        queue.addAircraft(a);
                        aircraftAddedToQueue = true;
                        break;
                    }
                }
                if (!aircraftAddedToQueue) {
                    throw new MalformedSaveException("Aircraft "
                            + "callsign not found in Loaded Aircraft");
                }
            }

            if (queueQuantity != queue.getAircraftInOrder().size()) {
                throw new MalformedSaveException("queue quantity didn't match the queued aircraft");
            }
        }
    }

    /**
     * <p>
     * Reads the map of currently loading aircraft from the given reader instance.
     *
     * <p>
     * Rather than returning a map, this method does not return anything.
     * Instead, it should modify the given map by adding entries (aircraft/integer pairs) to it.
     *
     * <p>
     * The contents of the text read from the reader should match the format specified in the
     * queuesWriter row of in the table shown in ViewModel.saveAs(). Note that this method should
     * only read the map of loading aircraft, not the takeoff queue or landing queue.
     * Reading these queues is handled in the readQueue(BufferedReader, List, AircraftQueue) method.
     *
     * <p>
     * For an example of valid encoded map of loading aircraft,
     * see the provided saves/queues_basic.txt and saves/queues_default.txt files.
     *
     * <p>
     * The contents read from the reader are invalid if any of the following conditions are true:
     * <ul>
     * <li>The first line read from the reader is null.
     * <li>The number of colons (:) detected on the first line is more/fewer than expected.
     * <li>The number of aircraft specified on the first line is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The number of aircraft is greater than zero and the second line read from the reader is
     * null.
     * <li>The number of aircraft specified on the first line is not equal to the number of
     * callsigns
     * read on the second line.
     * <li>For any callsign/loading time pair on the second line, the number of colons detected is
     * not equal to one. For example, ABC123:5:9 is invalid.
     * <li>A callsign listed on the second line does not correspond to the callsign of any aircraft
     * contained in the list of aircraft given as a parameter.
     * <li>Any ticksRemaining value on the second line is not an integer
     * (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>Any ticksRemaining value on the second line is less than one (1).
     * </ul>
     *
     * @param reader          reader from which to load the map of loading aircraft
     * @param aircraft        list of all aircraft, used when validating that callsigns exist
     * @param loadingAircraft empty map that aircraft and their loading times will be added to
     * @throws IOException            if the format of the text read from the reader is invalid
     *                                according to the rules above
     * @throws MalformedSaveException if an IOException is encountered when reading from the reader
     */

    public static void readLoadingAircraft(BufferedReader reader,
                                           List<Aircraft> aircraft,
                                           Map<Aircraft, Integer> loadingAircraft)
            throws IOException, MalformedSaveException {
        // read next line
        String possibleCallsignLine;
        while ((possibleCallsignLine = reader.readLine()) != null) {
            if (possibleCallsignLine == null || possibleCallsignLine.isEmpty()) {
                throw new MalformedSaveException("Invalid row read, was empty");
            }
            if (possibleCallsignLine.startsWith(LandingQueue.class.getSimpleName())
                    || possibleCallsignLine.startsWith("LoadingAircraft")
                    || possibleCallsignLine.startsWith(TakeoffQueue.class.getSimpleName())) {
                // completed this queue, move to next one.
                line = possibleCallsignLine; // this is a new queue
                return;
            }
            // should be on a callsign line, should be a comma separated list
            String[] callsigns = possibleCallsignLine.split(",");
            boolean aircraftAddedToQueue = false;
            for (String callsign : callsigns) {
                for (Aircraft a : aircraft) {
                    if (a.getCallsign().equals(callsign)) {
                        loadingAircraft.put(a, a.getLoadingTime());
                        aircraftAddedToQueue = true;
                        break;
                    }
                }
                if (!aircraftAddedToQueue) {
                    throw new MalformedSaveException("Aircraft "
                            + "callsign not found in Loaded Aircraft");
                }
            }
            if (queueQuantity != loadingAircraft.size()) {
                throw new MalformedSaveException("aircraft in queue number didn't match aircraft");
            }
        }

    }

    /**
     * <p>
     * Reads a terminal from the given string and reads its gates from the given reader instance.
     * <p>
     * The format of the given string and the text read from the reader should match the
     * encoded representation of a terminal, as described in Terminal.encode().
     *
     * <p>
     * For an example of valid encoded terminal with gates, see the provided
     * saves/terminalsWithGates_basic.txt and saves/terminalsWithGates_default.txt files.
     *
     * <p>
     * The encoded terminal is invalid if any of the following conditions are true:
     * <ul>
     * <li>The number of colons (:) detected on the first line is more/fewer than expected.
     * <li>The terminal type specified on the first line is neither AirplaneTerminal nor
     * HelicopterTerminal.
     * <li>The terminal number is not an integer (i.e. cannot be parsed by
     * Integer.parseInt(String)).
     * <li>The terminal number is less than one (1).
     * <li>The number of gates in the terminal is not an integer.
     * <li>The number of gates is less than zero or is greater than Terminal.MAX_NUM_GATES.
     * <li>A line containing an encoded gate was expected, but EOF (end of file) was received
     * (i.e. BufferedReader.readLine() returns null).
     * <li>Any of the conditions listed in the Javadoc for readGate(String, List) are true.
     * </ul>
     *
     * @param line     string containing the first line of the encoded terminal
     * @param reader   reader from which to load the gates of the terminal (subsequent lines)
     * @param aircraft list of all aircraft, used when validating that callsigns exist
     * @return decoded terminal with its gates added
     * @throws IOException            if the format of the given string or the text read from
     *                                the reader is invalid according to the rules above
     * @throws MalformedSaveException if an IOException is encountered when reading from the reader
     */

    public static Terminal readTerminal(String line,
                                        BufferedReader reader,
                                        List<Aircraft> aircraft)
            throws IOException, MalformedSaveException {

        // check there is exactly 3 colons ":" in the line
        long count = line.chars().filter(ch -> ch == ':').count();
        if (3 != count) {
            throw new MalformedSaveException("incorrect number of colon separators found in line");
        }
        if (line.endsWith(":") || line.startsWith(":")) {
            throw new MalformedSaveException("Line started or ended with a data separator, ':'");
        }
        String[] terminalData = line.split(":");

        // Terminal Type
        String terminalType = terminalData[0];
        if (!("AirplaneTerminal".equals(terminalType)
                || "HelicopterTerminal".equals(terminalType))) {
            throw new MalformedSaveException("invalid Terminal Type " + terminalType);
        }

        // Terminal Number
        String terminalNumber = terminalData[1];
        try {
            Integer.parseInt(terminalNumber);
        } catch (NumberFormatException n) {
            throw new MalformedSaveException("terminal Number " + terminalNumber
                    + " was not valid due to: " + n);
        }

        int terminalNumberParsed = Integer.parseInt(terminalNumber);
        if (terminalNumberParsed < 1) {
            throw new MalformedSaveException("terminal Number " + terminalNumber
                    + " was not valid since it was less than 1");
        }

        // Number of Gates
        String numberOfGates = terminalData[3];
        try {
            Integer.parseInt(numberOfGates);
        } catch (NumberFormatException n) {
            throw new MalformedSaveException("number of gates " + numberOfGates
                    + " was not valid due to: " + n);
        }

        int parsedNumberOfGates = Integer.parseInt(numberOfGates);

        if (parsedNumberOfGates < 0 || parsedNumberOfGates > Terminal.MAX_NUM_GATES) {
            throw new MalformedSaveException("terminal Number " + terminalNumber
                    + " was not valid since it was less than 1");
        }


        String gateLine = "";
        List<Gate> gates = new ArrayList<>();
        while ((gateLine = reader.readLine()) != null) {
            if (gateLine.length() == 0) {
                throw new MalformedSaveException("Gate Line read was length zero.");
            }
            String firstChar = gateLine.substring(0, 1);
            // keep adding gates while the firstChar is a number.
            try {
                Integer.parseInt(firstChar);
                gates.add(readGate(gateLine, aircraft));
            } catch (NumberFormatException e) {
                // this is it for this Terminal.
                break;
            }
        }

        Terminal terminal = null;
        if ("AirplaneTerminal".equals(terminalType)) {
            terminal = new AirplaneTerminal(terminalNumberParsed);
            for (Gate gate : gates) {
                try {
                    terminal.addGate(gate);
                } catch (NoSpaceException e) {
                    throw new MalformedSaveException("unable to add gate due to space issue, " + e);
                }
            }

        }
        if ("HelicopterTerminal".equals(terminalType)) {
            terminal = new HelicopterTerminal(terminalNumberParsed);
            for (Gate gate : gates) {
                try {
                    terminal.addGate(gate);
                } catch (NoSpaceException e) {
                    e.printStackTrace();
                }
            }

        }

        String emergencyStatus = terminalData[2];
        if (emergencyStatus.equals("true")) {
            terminal.declareEmergency();
        }

        if (!(emergencyStatus.equals("false") || emergencyStatus.equals("true"))) {
            throw new MalformedSaveException("emergency status was '" + emergencyStatus
                    + "' which is neither 'true or 'false'");
        }
        recursivelyPopulatedTerminals.add(terminal);
        if (gateLine != null) {
            // recusive populate the rest of the terminals in the 1 read.
            readTerminal(gateLine, reader, aircraft);
        }
        // don't really need the returned first aircraft read here.
        return terminal;
    }

    /**
     * <p>
     * Reads a gate from its encoded representation in the given string.
     *
     * <p>
     * The format of the string should match the encoded representation of a gate,
     * as described in Gate.encode().
     *
     * <p>
     * The encoded string is invalid if any of the following conditions are true:
     * <ul>
     * <li>The number of colons (:) detected was more/fewer than expected.
     * <li>The gate number is not an integer (i.e. cannot be parsed by Integer.parseInt(String)).
     * <li>The gate number is less than one (1).
     * <li>The callsign of the aircraft parked at the gate is not empty and the callsign does not
     * correspond to the callsign of any aircraft contained in the list of aircraft given
     * as a parameter.
     * </ul>
     *
     * @param line     string containing the encoded gate
     * @param aircraft list of all aircraft, used when validating that callsigns exist
     * @return decoded gate instance
     * @throws MalformedSaveException if the format of the given string is invalid according to
     *                                the rules above
     */
    public static Gate readGate(String line, List<Aircraft> aircraft)
            throws MalformedSaveException {
        // check there is exactly 3 colons ":" in the line
        long count = line.chars().filter(ch -> ch == ':').count();
        if (1 != count) {
            throw new MalformedSaveException("incorrect number of colon separators found in line");
        }
        if (line.endsWith(":") || line.startsWith(":")) {
            throw new MalformedSaveException("Line started or ended with a data separator, ':'");
        }

        String[] gateData = line.split(":");
        String gateNumber = gateData[0];
        String gateAircraftCallsign = gateData[1];


        try {
            Integer.parseInt(gateNumber);
        } catch (NumberFormatException n) {
            throw new MalformedSaveException("Gate number " + gateNumber
                    + " was not valid due to: " + n);
        }

        int gateNumberParsed = Integer.parseInt(gateNumber);
        if (gateNumberParsed < 1) {
            throw new MalformedSaveException("gate Number " + gateNumberParsed
                    + " was not valid since it was less than 1");
        }

        //System.out.println(line);
        Gate gate = new Gate(gateNumberParsed);

        if ("empty".equals(gateAircraftCallsign)) {
            return gate;
        }

        // if not empty, check if callsign matches and if so park and return the gate with it.
        for (Aircraft a : aircraft) {
            if (a.getCallsign().equals(gateAircraftCallsign)) {
                try {
                    gate.parkAircraft(a);
                    return gate;
                } catch (NoSpaceException e) {
                    e.printStackTrace();
                }
            }
        }

        // if we reached here, then callsign didnt match an aircraft
        throw new MalformedSaveException("Aircraft callsign " + gateAircraftCallsign
                + " did not match any in " + aircraft);

    }

}
