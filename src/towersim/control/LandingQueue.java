package towersim.control;

import towersim.aircraft.Aircraft;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a rule-based queue of aircraft waiting in the air to land.
 * <p>
 * The rules in the landing queue are designed to ensure that aircraft
 * are prioritised for landing based on "urgency" factors such as remaining
 * fuel onboard, emergency status and cargo type.
 */
public class LandingQueue extends AircraftQueue {

    /**
     * holds the list of aircraft in the landing queue
     */
    private List<Aircraft> landingQueue = new ArrayList<>();

    /**
     * Constructs a new LandingQueue with an initially empty queue of aircraft.
     */
    public LandingQueue() {
    }

    /**
     * Adds the given aircraft to the queue.
     *
     * @param aircraft aircraft to add to queue
     */
    @Override
    public void addAircraft(Aircraft aircraft) {
        landingQueue.add(aircraft);
    }


    /**
     * <p>
     * Removes and returns the aircraft at the front of the queue. Returns null if the
     * queue is empty.
     *
     * <p>
     * The same rules as described in peekAircraft() should be used for determining which aircraft
     * to remove and return.
     *
     * @return aircraft at front of queue
     */
    @Override
    public Aircraft removeAircraft() {
        // call peek first to find which to remove, then remove it and return it
        Aircraft aircraftToRemove = peekAircraft();
        landingQueue.remove(aircraftToRemove);
        return aircraftToRemove;
    }

    /**
     * <p>
     * Returns the aircraft at the front of the queue without removing it from the queue, or null
     * if the queue is empty.
     *
     * <p>
     * The rules for determining which aircraft in the queue should be returned next are as follows:
     * <ol>
     * <li> If an aircraft is currently in a state of emergency, it should be returned.
     * If more than one aircraft are in an emergency, return the one added to the queue first.
     * <li>If an aircraft has less than or equal to 20 percent fuel remaining, a critical level,
     * it should be returned (see Aircraft.getFuelPercentRemaining()).
     * If more than one aircraft have a critical level of fuel onboard, return the one added to
     * the queue first.
     * <li>If there are any passenger aircraft in the queue, return the passenger aircraft that
     * was added to the
     * queue first.
     * <li>If this point is reached and no aircraft has been returned, return the aircraft that
     * was added to the
     * queue first.
     * </ol>
     *
     * @return aircraft at front of queue
     */
    @Override
    public Aircraft peekAircraft() {
        // get aircraft list in emergency state and return first
        List<Aircraft> emergencyAircraft = new ArrayList<>();
        for (Aircraft aircraft : landingQueue) {
            if (aircraft.hasEmergency()) {
                emergencyAircraft.add(aircraft);
            }
        }
        if (emergencyAircraft.size() != 0) {
            return emergencyAircraft.get(0); // "added first"? is this ok?
        }

        // get aircraft list in low fuel state and return first
        List<Aircraft> lowFuelAircraft = new ArrayList<>();
        for (Aircraft aircraft : landingQueue) {
            if (aircraft.getFuelAmount() < 20.0) {
                lowFuelAircraft.add(aircraft);
            }
        }
        if (lowFuelAircraft.size() != 0) {
            return lowFuelAircraft.get(0); // "added first"? is this ok?
        }

        // get aircraft list that are passenger aircraft and return first
        List<Aircraft> passengerAircraft = new ArrayList<>();
        for (Aircraft aircraft : landingQueue) {
            if (aircraft.getCharacteristics().passengerCapacity > 0) {
                passengerAircraft.add(aircraft);
            }
        }
        if (passengerAircraft.size() != 0) {
            return passengerAircraft.get(0); // "added first"? is this ok?
        }

        // get aircraft list that are freight aircraft and return first
        List<Aircraft> freightAircraft = new ArrayList<>();
        for (Aircraft aircraft : landingQueue) {
            if (aircraft.getCharacteristics().freightCapacity > 0) {
                freightAircraft.add(aircraft);
            }
        }
        if (freightAircraft.size() != 0) {
            return freightAircraft.get(0); // "added first"? is this ok?
        }
        if (landingQueue.size() != 0) {
            return landingQueue.get(0);
        }
        return null;
    }

    /**
     * <p>
     * Returns a list containing all aircraft in the queue, in order.
     * <p>
     * That is, the first element of the returned list should be
     * the first aircraft that would be returned by calling removeAircraft(), and so on.
     *
     * <p>
     * Adding or removing elements from the returned list should not affect the original queue.
     *
     * @return list of all aircraft in queue, in queue order
     */
    @Override
    public List<Aircraft> getAircraftInOrder() {
        // temporarily store the data since calling remove will affect this instance of
        // landing queue and its Aircraft list
        List<Aircraft> currentLandingQueue = new ArrayList<>(landingQueue);
        List<Aircraft> peekedAndRemovedAircraft = new ArrayList<>();
        int landingSize = landingQueue.size();
        for (int i = 0; i < landingSize; i++) {
            Aircraft removedAircraft = this.removeAircraft();
            peekedAndRemovedAircraft.add(removedAircraft);
        }
        // set the landing queue back to its original state
        this.landingQueue = currentLandingQueue;
        // return the ordered list of aircraft
        return peekedAndRemovedAircraft;
    }

    /**
     * <p>
     * Returns true if the given aircraft is in the queue.
     *
     * @param aircraft aircraft to find in queue
     * @return true if aircraft is in queue; false otherwise
     */
    @Override
    public boolean containsAircraft(Aircraft aircraft) {
        return landingQueue.contains(aircraft);
    }

}
