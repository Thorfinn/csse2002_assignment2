package towersim.control;

import org.junit.Before;
import org.junit.Test;
import towersim.aircraft.Aircraft;
import towersim.aircraft.AircraftCharacteristics;
import towersim.aircraft.FreightAircraft;
import towersim.aircraft.PassengerAircraft;
import towersim.tasks.Task;
import towersim.tasks.TaskList;
import towersim.tasks.TaskType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LandingQueueTest {
    private LandingQueue landingQueue = new LandingQueue();
    ;
    private PassengerAircraft passengerAircraft1;
    private PassengerAircraft passengerAircraft2;
    private PassengerAircraft passengerAircraft3;
    private FreightAircraft freightAircraft1;
    private FreightAircraft freightAircraft2;
    private FreightAircraft freightAircraft3;
    private final TaskList taskList1 = new TaskList(List.of(
            new Task(TaskType.AWAY),
            new Task(TaskType.LAND),
            new Task(TaskType.LOAD),
            new Task(TaskType.TAKEOFF)));

    @Before
    public void setup() {
        passengerAircraft1 = new PassengerAircraft("UTD302",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        passengerAircraft2 = new PassengerAircraft("UPS119",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        passengerAircraft3 = new PassengerAircraft("ABC123",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        freightAircraft1 = new FreightAircraft("ABC001", AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.6,
                AircraftCharacteristics.BOEING_747_8F.freightCapacity);

        freightAircraft2 = new FreightAircraft("ABC002", AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.6,
                60000);

        freightAircraft3 = new FreightAircraft("EMP001", AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity / 2, 0);
    }

    @Test
    public void test_addAircraft() {
        landingQueue.addAircraft(passengerAircraft1);
        assertTrue(landingQueue.getAircraftInOrder().size() == 1);
        assertTrue(landingQueue.getAircraftInOrder().contains(passengerAircraft1));
    }

    @Test
    public void test_removeAircraft() {
        landingQueue.addAircraft(passengerAircraft1);
        assertEquals(1, landingQueue.getAircraftInOrder().size());
        landingQueue.removeAircraft();
        assertEquals(0, landingQueue.getAircraftInOrder().size());
    }

    @Test
    public void test_peekAircraft_1() {
        // test state of emergency priority on aircraft 2
        landingQueue.addAircraft(passengerAircraft1);
        passengerAircraft2.declareEmergency();
        landingQueue.addAircraft(passengerAircraft2);
        landingQueue.addAircraft(passengerAircraft3);
        Aircraft aircraft = landingQueue.peekAircraft();
        assertEquals(aircraft, passengerAircraft2);
    }

    @Test
    public void test_peekAircraft_2() {
        // test low fuel on aircraft 3
        passengerAircraft3 = new PassengerAircraft("UPS119",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                5, // 5% fuel so should be prioritised
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        landingQueue.addAircraft(passengerAircraft1);
        landingQueue.addAircraft(passengerAircraft2);
        landingQueue.addAircraft(passengerAircraft3);
        Aircraft aircraft = landingQueue.peekAircraft();
        assertEquals(aircraft, passengerAircraft3);
    }

    @Test
    public void test_peekAircraft_3() {
        // test emergency and low fuel on aircraft 3
        passengerAircraft3 = new PassengerAircraft("UPS119",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                5, // 5% fuel so should be prioritised
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        landingQueue.addAircraft(passengerAircraft1);
        passengerAircraft2.declareEmergency();
        landingQueue.addAircraft(passengerAircraft2);
        landingQueue.addAircraft(passengerAircraft3);
        Aircraft aircraft = landingQueue.peekAircraft();
        assertEquals(aircraft, passengerAircraft2);
    }

    @Test
    public void test_peekAircraft_4() {
        // no emergency no low fuel - should return the first passenger aircraft added.
        landingQueue.addAircraft(passengerAircraft1);
        landingQueue.addAircraft(passengerAircraft2);
        landingQueue.addAircraft(passengerAircraft3);
        Aircraft aircraft = landingQueue.peekAircraft();
        assertEquals(aircraft, passengerAircraft1);
    }

    @Test
    public void test_peekAircraft_5() {
        // no emergency, no low fuel, no passengerAircraft -
        // should return the first freight aircraft added.
        landingQueue.addAircraft(freightAircraft1);
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        Aircraft aircraft = landingQueue.peekAircraft();
        assertEquals(aircraft, freightAircraft1);
    }

    @Test
    public void test_getAircraftInOrder_1() {

        landingQueue.addAircraft(freightAircraft1);
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        List<Aircraft> orderedAircraftList = new ArrayList<>();
        orderedAircraftList.add(freightAircraft1);
        orderedAircraftList.add(freightAircraft2);
        orderedAircraftList.add(freightAircraft3);
        List<Aircraft> aircraftsTest = landingQueue.getAircraftInOrder();
        assertEquals(aircraftsTest, orderedAircraftList);
    }

    @Test
    public void test_getAircraftInOrder_2() {
        freightAircraft3 = new FreightAircraft("ABC001",
                AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.1,
                AircraftCharacteristics.BOEING_747_8F.freightCapacity);
        landingQueue.addAircraft(freightAircraft1);
        freightAircraft2.declareEmergency();
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        List<Aircraft> orderedAircraftList = new ArrayList<>();
        orderedAircraftList.add(freightAircraft2);
        orderedAircraftList.add(freightAircraft3);
        orderedAircraftList.add(freightAircraft1);
        List<Aircraft> aircraftsTest = landingQueue.getAircraftInOrder();
        assertEquals(aircraftsTest, orderedAircraftList);
    }

    @Test
    public void test_containsAircraft_1() {
        freightAircraft3 = new FreightAircraft("ABC001",
                AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.1,
                AircraftCharacteristics.BOEING_747_8F.freightCapacity);
        landingQueue.addAircraft(freightAircraft1);
        freightAircraft2.declareEmergency();
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        assertTrue(landingQueue.containsAircraft(freightAircraft3));
    }

    @Test
    public void test_containsAircraft_2() {
        freightAircraft3 = new FreightAircraft("ABC001",
                AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.1,
                AircraftCharacteristics.BOEING_747_8F.freightCapacity);
        landingQueue.addAircraft(freightAircraft1);
        freightAircraft2.declareEmergency();
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        assertFalse(landingQueue.containsAircraft(passengerAircraft1));
    }

    @Test
    public void test_containsAircraft_3() {
        freightAircraft3 = new FreightAircraft("ABC001",
                AircraftCharacteristics.BOEING_747_8F,
                taskList1,
                AircraftCharacteristics.BOEING_747_8F.fuelCapacity * 0.1,
                AircraftCharacteristics.BOEING_747_8F.freightCapacity);
        landingQueue.addAircraft(freightAircraft1);
        freightAircraft2.declareEmergency();
        landingQueue.addAircraft(freightAircraft2);
        landingQueue.addAircraft(freightAircraft3);
        assertFalse(landingQueue.containsAircraft(null));
    }

}
