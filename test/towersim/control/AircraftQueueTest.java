package towersim.control;

import org.junit.Test;
import towersim.aircraft.Aircraft;
import towersim.aircraft.AircraftCharacteristics;
import towersim.aircraft.FreightAircraft;
import towersim.aircraft.PassengerAircraft;
import towersim.tasks.Task;
import towersim.tasks.TaskList;
import towersim.tasks.TaskType;

import java.util.ArrayList;
import java.util.List;

public class AircraftQueueTest {
    private TaskList taskList1;

    private Aircraft dummyAircraft1;
    private Aircraft dummyAircraft2;

    private Aircraft passengerAircraft1;
    private Aircraft passengerAircraft2;
    /*
     * Dummy AircraftQueue that don't extend LandingQueue or TakeoffQueue, useful for testing
     * methods overridden in Aircraft subclasses
     */
    class DummyAircraftQueue extends AircraftQueue {
        private final List<Aircraft> dummyAircraftQueue = new ArrayList<>();

        public DummyAircraftQueue() {
            super();
        }


        @Override
        public void addAircraft(Aircraft aircraft) {
            this.getAircraftInOrder().add(aircraft);
        }

        @Override
        public Aircraft removeAircraft() {
            return this.getAircraftInOrder().remove(0);
        }

        @Override
        public Aircraft peekAircraft() {
            return null;
        }

        @Override
        public List<Aircraft> getAircraftInOrder() {
            // do the ordering later...
            return dummyAircraftQueue;
        }

        @Override
        public boolean containsAircraft(Aircraft aircraft) {
            return dummyAircraftQueue.contains(aircraft);
        }
    }

    @Test
    public void toString_Test1() {
        DummyAircraftQueue da = new DummyAircraftQueue();
        this.taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        this.passengerAircraft1 = new PassengerAircraft("ABC123",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        da.addAircraft(passengerAircraft1);
        da.addAircraft(passengerAircraft1);
        System.out.println(da.toString());
    }

    @Test
    public void encode_Test1(){
        DummyAircraftQueue da = new DummyAircraftQueue();
        this.taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        this.passengerAircraft1 = new PassengerAircraft("ABC123",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        da.addAircraft(passengerAircraft1);
        da.addAircraft(passengerAircraft1);
        System.out.println(da.encode());
    }


}
