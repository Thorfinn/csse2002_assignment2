package towersim.control;

import org.junit.Test;
import towersim.aircraft.Aircraft;
import towersim.aircraft.AircraftCharacteristics;
import towersim.aircraft.PassengerAircraft;
import towersim.tasks.Task;
import towersim.tasks.TaskList;
import towersim.tasks.TaskType;
import towersim.util.MalformedSaveException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;


public class ControlTowerInitialiserTest {

    @Test(expected = MalformedSaveException.class)
    public void loadTerminalsWithGates_InvalidTest() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "AirplaneTerminal:notATerminalNumber:false:0" // invalid terminal number
        );
        ControlTowerInitialiser.loadTerminalsWithGates(
                new StringReader(fileContents), List.of());
    }

    // test incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadTerminalsWithGates_InvalidTest2() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "AirplaneTerminal::notATerminalNumber:false:0" // invalid terminal number
        );
        try {
            ControlTowerInitialiser.loadTerminalsWithGates(
                    new StringReader(fileContents), List.of());
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // terminal Number notATerminalNumber was not valid due to: java.lang.NumberFormatException
    @Test(expected = MalformedSaveException.class)
    public void loadTerminalsWithGates_InvalidTest3() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "AirplaneTerminal:notATerminalNumber:false:0" // invalid terminal number
        );
        try {
            ControlTowerInitialiser.loadTerminalsWithGates(
                    new StringReader(fileContents), List.of());
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // terminal Number didn't match list entry quantity entries
    @Test(expected = MalformedSaveException.class)
    public void loadTerminalsWithGates_InvalidTest4() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "7",
                "AirplaneTerminal:1:false:0" // 7 doesn't match only 1 row
        );
        try {
            ControlTowerInitialiser.loadTerminalsWithGates(
                    new StringReader(fileContents), List.of());
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // aircraft Number didn't match list entries of Aircraft
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest1() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "7", // should be 8
                "QFA481:AIRBUS_A320:AWAY,AWAY,LAND,WAIT,LOAD@60,TAKEOFF,AWAY:10000.00:false:132",
                "UTD302:BOEING_787:WAIT,LOAD@100,TAKEOFF,AWAY,AWAY,AWAY,LAND:10000.00:false:0",
                "UPS119:BOEING_747_8F:WAIT,LOAD@50,TAKEOFF,AWAY,AWAY,AWAY,LAND:4000.00:false:0",
                "VH-BF1:ROBINSON_R44:LAND,WAIT,LOAD@75,TAKEOFF,AWAY,AWAY:95.00:true:4",
                "VH-BF2:ROBINSON_R44:LAND,WAIT,LOAD@75,TAKEOFF,AWAY,AWAY:90.00:false:4",
                "VH-BF3:ROBINSON_R44:LAND,WAIT,LOAD@75,TAKEOFF,AWAY,AWAY:40.00:true:4",
                "VH-BF4:ROBINSON_R44:LAND,WAIT,LOAD@75,TAKEOFF,AWAY,AWAY:40.00:false:4",
                "VH-BF5:ROBINSON_R44:LAND,WAIT,LOAD@75,TAKEOFF,AWAY,AWAY:10.00:false:4"

        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest2() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:WhatIsThis:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:10000.00:false:132"
                // WhatIsThis - extra parameter, therefore more colons ':' (6) than required (5)
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest3() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320::10000.00:false:132"
                // Missing Content - double colons '::' (still with 5 colons)
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }


    // incorrect format of fuel
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest4_fuel() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:cat:false:132"
                // invalid Fuel format
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }


    // incorrect format of freight or passengers
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest5_freight() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:1000.0:false:Cat"
                // invalid Freight/Fuel format
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect format of freight or passengers
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest6_freight() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:1000.0:false:10000"
                // invalid Freight/Fuel amount
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect format of freight or passengers
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest7_freight() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:1000.0:false:-10000"
                // invalid Freight/Fuel amount
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect format of freight or passengers
    @Test(expected = MalformedSaveException.class)
    public void readTaskList_InvalidTest1_null() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320::1000.0:false:-10000"
                // invalid Freight/Fuel amount
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect format of freight or passengers
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest8_freight() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:AWAY,LAND,WAIT,LOAD@60,TAKEOFF:1000.0:cat:100"
                // invalid emergency
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest4() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:NOT_A_TASK:10000.00:false:132"
                // Invalid Task Type
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest5() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                ":AIRBUS_A320:NOT_A_TASK:10000.00:false:132"
                // Started or ended with a colon ':'
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }


    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest6() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:NOT_AN_AIRCRAFT_TYPE:LOAD:10000.00:false:132"
                // not an aircraft type
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest7() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:WAIT,WAIT:100000.00:false:132"
                // too much fuel
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest8() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:WAIT,WAIT:-10.00:false:132"
                // negative fuel
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
//    @Test(expected = MalformedSaveException.class)
//    public void loadAircraft_InvalidTest9() throws IOException, MalformedSaveException {
//        String fileContents = String.join(System.lineSeparator(),
//                "1",
//                "QFA481:AIRBUS_A320:WAIT,WAIT:1000.00:cat:132"
//                // neither 'true' or 'false' for emergency status
//        );
//        try {
//            ControlTowerInitialiser.loadAircraft(
//                    new StringReader(fileContents));
//        } catch (MalformedSaveException e) {
//            System.out.println(e);
//            throw new MalformedSaveException(e);
//        }
//    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest10() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:WAIT,WAIT:1000.00:true:600"
                // too many passnegers (max 150)
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest11() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                ""
                // nothing
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest12() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "hi: : : : :hello"
                // rubbish
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest13() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOAD:10000.00:false:132"
                // invalid LOAD task with no percentage (missing @ symbol and amount)
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest14() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOAD@@100:10000.00:false:132"
                // invalid - had multiple @ symbols for LOAD task
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest15() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOAD@150:10000.00:false:132"
                // invalid fright or passengers
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest16() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOAD@CAT:10000.00:false:132"
                // invalid load percent, not a number
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest17() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOADY@50:10000.00:false:132"
                // invalid load task, not a valid Task type
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // incorrect number of colon separators found in line
    @Test(expected = MalformedSaveException.class)
    public void loadAircraft_InvalidTest18() throws IOException, MalformedSaveException {
        String fileContents = String.join(System.lineSeparator(),
                "1",
                "QFA481:AIRBUS_A320:LOAD@50:cat:false:132"
                // invalid load task, not a valid Task type
        );
        try {
            ControlTowerInitialiser.loadAircraft(
                    new StringReader(fileContents));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // Callsign not found in the aircraft list
    @Test(expected = MalformedSaveException.class)
    public void readLoadingAircraft_InvalidTest1() throws IOException, MalformedSaveException {
        List<Aircraft> aircraft = new ArrayList<>();
        TaskList taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        PassengerAircraft passengerAircraft1 = new PassengerAircraft("UTD302",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        PassengerAircraft passengerAircraft2 = new PassengerAircraft("HK-123",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        aircraft.add(passengerAircraft1);
        aircraft.add(passengerAircraft2);

        String fileContents = String.join(System.lineSeparator(),
                "TakeoffQueue:0",
                "LandingQueue:6",
                "VH-BF1,VH-BF2,VH-BF3,VH-BF4,VH-BF5,HK-123",
                "LoadingAircraft:0"
        );
        try {
            ControlTowerInitialiser.loadQueues(
                    new StringReader(fileContents),
                    aircraft,
                    new TakeoffQueue(),
                    new LandingQueue(),
                    new TreeMap<>(Comparator.comparing(Aircraft::getCallsign)));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // not matching quantity for landing queue
    @Test(expected = MalformedSaveException.class)
    public void readLoadingAircraft_InvalidTest2() throws IOException, MalformedSaveException {
        List<Aircraft> aircraft = new ArrayList<>();
        TaskList taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        PassengerAircraft passengerAircraft1 = new PassengerAircraft("VH-BF2",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        PassengerAircraft passengerAircraft2 = new PassengerAircraft("VH-BF1",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        aircraft.add(passengerAircraft1);
        aircraft.add(passengerAircraft2);

        String fileContents = String.join(System.lineSeparator(),
                "TakeoffQueue:0",
                "LandingQueue:18", // not matching quantity for landing queue
                "VH-BF1,VH-BF2",
                "LoadingAircraft:0"
        );
        try {
            ControlTowerInitialiser.loadQueues(
                    new StringReader(fileContents),
                    aircraft,
                    new TakeoffQueue(),
                    new LandingQueue(),
                    new TreeMap<>(Comparator.comparing(Aircraft::getCallsign)));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // not matching quantity for Loading queue
    @Test(expected = MalformedSaveException.class)
    public void readLoadingAircraft_InvalidTest3() throws IOException, MalformedSaveException {
        List<Aircraft> aircraft = new ArrayList<>();
        TaskList taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        PassengerAircraft passengerAircraft1 = new PassengerAircraft("VH-BF2",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        PassengerAircraft passengerAircraft2 = new PassengerAircraft("VH-BF1",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        aircraft.add(passengerAircraft1);
        aircraft.add(passengerAircraft2);

        String fileContents = String.join(System.lineSeparator(),
                "TakeoffQueue:0",
                "LandingQueue:0",
                "LoadingAircraft:18", // not matching quantity for Loading queue
                "VH-BF1,VH-BF2"
        );
        try {
            ControlTowerInitialiser.loadQueues(
                    new StringReader(fileContents),
                    aircraft,
                    new TakeoffQueue(),
                    new LandingQueue(),
                    new TreeMap<>(Comparator.comparing(Aircraft::getCallsign)));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }

    // not matching quantity for Loading queue
    @Test(expected = MalformedSaveException.class)
    public void readLoadingAircraft_InvalidTest4() throws IOException, MalformedSaveException {
        List<Aircraft> aircraft = new ArrayList<>();
        TaskList taskList1 = new TaskList(List.of(
                new Task(TaskType.AWAY),
                new Task(TaskType.LAND),
                new Task(TaskType.LOAD),
                new Task(TaskType.TAKEOFF)));

        PassengerAircraft passengerAircraft1 = new PassengerAircraft("VH-BF2",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        PassengerAircraft passengerAircraft2 = new PassengerAircraft("VH-BF1",
                AircraftCharacteristics.AIRBUS_A320,
                taskList1,
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
        aircraft.add(passengerAircraft1);
        aircraft.add(passengerAircraft2);

        String fileContents = String.join(System.lineSeparator(),
                "TakeoffQueue:10",
                "VH-BF1,VH-BF2",
                "LandingQueue:0",
                "LoadingAircraft:0" // not matching quantity for Loading queue
        );
        try {
            ControlTowerInitialiser.loadQueues(
                    new StringReader(fileContents),
                    aircraft,
                    new TakeoffQueue(),
                    new LandingQueue(),
                    new TreeMap<>(Comparator.comparing(Aircraft::getCallsign)));
        } catch (MalformedSaveException e) {
            System.out.println(e);
            throw new MalformedSaveException(e);
        }
    }


    /*

    ---- Actual File Testing ----

    private ControlTowerInitialiser towerInit;

    @Before
    public void setup() {
        towerInit = new ControlTowerInitialiser();

    }


        @Test
        public void testLoadTicks() {
            try {
                Reader reader = new FileReader("C:\\Git\\assignment2_csse2002\\saves\\tick_basic.txt");
                long testLong = towerInit.loadTick(reader);
                System.out.println("testLong" + testLong);
                assertTrue(testLong >= 0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedSaveException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Test
        public void testLoadAircraft() {
            try {
                Reader reader = new FileReader("C:\\Git\\assignment2_csse2002\\saves\\aircraft_basic.txt");
                List<Aircraft> testLoadAircraft = towerInit.loadAircraft(reader);
                System.out.println("testLoadAircraft" + testLoadAircraft);
                //assertTrue(testLong >= 0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedSaveException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Test
        public void testloadTerminalsWithGates() {
            try {
                Reader reader = new FileReader(
                        "C:\\Git\\assignment2_csse2002\\saves\\terminalsWithGates_basic.txt");
                List<Aircraft> aircraft = new ArrayList<>();
                TaskList taskList1 = new TaskList(List.of(
                        new Task(TaskType.AWAY),
                        new Task(TaskType.LAND),
                        new Task(TaskType.LOAD),
                        new Task(TaskType.TAKEOFF)));

                PassengerAircraft passengerAircraft1 = new PassengerAircraft("UTD302",
                        AircraftCharacteristics.AIRBUS_A320,
                        taskList1,
                        AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                        AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

                PassengerAircraft passengerAircraft2 = new PassengerAircraft("UPS119",
                        AircraftCharacteristics.AIRBUS_A320,
                        taskList1,
                        AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                        AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

                PassengerAircraft passengerAircraft3 = new PassengerAircraft("ABC123",
                        AircraftCharacteristics.AIRBUS_A320,
                        taskList1,
                        AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                        AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
                aircraft.add(passengerAircraft1);
                aircraft.add(passengerAircraft2);
                aircraft.add(passengerAircraft3);
                List<Terminal> testLoadTerminalsWithGates = ControlTowerInitialiser.loadTerminalsWithGates(new BufferedReader(reader), aircraft);
                System.out.println("testLoadTerminalsWithGates" + testLoadTerminalsWithGates);
                //assertTrue(testLong >= 0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedSaveException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    */
/*
    @Test
    public void testloadQueues() {
        try {
            Reader reader = new FileReader("C:\\Git\\assignment2_csse2002\\saves\\queues_basic.txt");
            List<Aircraft> aircraft = new ArrayList<>();
            TaskList taskList1 = new TaskList(List.of(
                    new Task(TaskType.AWAY),
                    new Task(TaskType.LAND),
                    new Task(TaskType.LOAD),
                    new Task(TaskType.TAKEOFF)));

            PassengerAircraft passengerAircraft1 = new PassengerAircraft("UTD302",
                    AircraftCharacteristics.AIRBUS_A320,
                    taskList1,
                    AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                    AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

            PassengerAircraft passengerAircraft2 = new PassengerAircraft("HK-123",
                    AircraftCharacteristics.AIRBUS_A320,
                    taskList1,
                    AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                    AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

            PassengerAircraft passengerAircraft3 = new PassengerAircraft("VH-BFK",
                    AircraftCharacteristics.AIRBUS_A320,
                    taskList1,
                    AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                    AircraftCharacteristics.AIRBUS_A320.passengerCapacity);
            aircraft.add(passengerAircraft1);
            aircraft.add(passengerAircraft2);
            aircraft.add(passengerAircraft3);
            TakeoffQueue toq = new TakeoffQueue();
            LandingQueue lq = new LandingQueue();
            Map<Aircraft, Integer> loadingAircraft = new HashMap<>();
            ControlTowerInitialiser.loadQueues(new BufferedReader(reader), aircraft, toq, lq, loadingAircraft);
            System.out.println("testLoadTerminalsWithGates");
            //assertTrue(testLong >= 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedSaveException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
*/

}
