package towersim.ground;

import org.junit.Before;
import org.junit.Test;
import towersim.aircraft.Aircraft;
import towersim.aircraft.AircraftCharacteristics;
import towersim.aircraft.PassengerAircraft;
import towersim.tasks.Task;
import towersim.tasks.TaskList;
import towersim.tasks.TaskType;
import towersim.util.NoSpaceException;

import java.util.List;

import static org.junit.Assert.*;

public class GateTest {
    private Gate gate;
    private Aircraft aircraft1;
    private Aircraft aircraft2;

    @Before
    public void setup() {
        this.gate = new Gate(2);
        this.aircraft1 = new PassengerAircraft("ABC123", AircraftCharacteristics.AIRBUS_A320,
                new TaskList(List.of(
                        new Task(TaskType.AWAY),
                        new Task(TaskType.LAND),
                        new Task(TaskType.LOAD),
                        new Task(TaskType.TAKEOFF))),
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity);

        this.aircraft2 = new PassengerAircraft("XYZ987", AircraftCharacteristics.AIRBUS_A320,
                new TaskList(List.of(
                        new Task(TaskType.AWAY),
                        new Task(TaskType.LAND),
                        new Task(TaskType.WAIT),
                        new Task(TaskType.LOAD),
                        new Task(TaskType.TAKEOFF))),
                AircraftCharacteristics.AIRBUS_A320.fuelCapacity / 2,
                AircraftCharacteristics.AIRBUS_A320.passengerCapacity / 2);
    }

    @Test
    public void getGateNumber_Test() {
        assertEquals("getGateNumber() should return the gate number passed to Gate(int)",
                2, gate.getGateNumber());
    }

    @Test
    public void isOccupied_FalseTest() {
        assertFalse("isOccupied() should return false for a newly created gate",
                gate.isOccupied());
    }

    @Test
    public void isOccupied_TrueTest() {
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }
        assertTrue("isOccupied() should return true after an aircraft has been added to a gate",
                gate.isOccupied());
    }

    @Test
    public void parkAircraft_OccupiedTest() {
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }
        try {
            gate.parkAircraft(aircraft1);
            fail("parkAircraft() should throw an exception if the gate is already occupied");
        } catch (NoSpaceException expected) {}
    }

    @Test
    public void aircraftLeaves_OccupiedTest() {
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }
        gate.aircraftLeaves();
        assertFalse("isOccupied() should return false after calling aircraftLeaves()",
                gate.isOccupied());
        assertNull("getAircraftAtGate() should return null after calling aircraftLeaves()",
                gate.getAircraftAtGate());
    }

    @Test
    public void aircraftLeaves_UnOccupiedTest() {
        gate.aircraftLeaves();
        assertFalse("isOccupied() should return false after calling aircraftLeaves()",
                gate.isOccupied());
        assertNull("getAircraftAtGate() should return null after calling aircraftLeaves()",
                gate.getAircraftAtGate());
    }

    @Test
    public void getAircraftAtGate_OccupiedTest() {
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }

        assertEquals("getAircraftAtGate() should return the aircraft added via parkAircraft()",
                aircraft1, gate.getAircraftAtGate());
    }

    @Test
    public void getAircraftAtGate_UnoccupiedTest() {
        assertNull("getAircraftAtGate() should return null if no aircraft have been parked yet",
                gate.getAircraftAtGate());
    }

    @Test
    public void toString_UnoccupiedTest() {
        assertEquals("Gate 2 [empty]", gate.toString());
    }

    @Test
    public void toString_OccupiedTest() {
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }

        assertEquals("Gate 2 [ABC123]", gate.toString());
        gate.aircraftLeaves();

        try {
            gate.parkAircraft(aircraft2);
        } catch (NoSpaceException e) {
            fail("parkAircraft() should not throw an exception if the gate is unoccupied");
        }

        assertEquals("Gate 2 [XYZ987]", gate.toString());
    }

    @Test
    public void equals_Test1_equal() {
        Gate gate = new Gate(1);
        Gate gateCopy = new Gate(1);
        boolean test = gate.equals(gateCopy);
        assertTrue(test);
    }

    @Test
    public void equals_Test2_notEqual() {
        Gate gate = new Gate(1);
        Gate gateCopy = new Gate(2);
        boolean test = gate.equals(gateCopy);
        assertFalse(test);
    }

    @Test
    public void equals_Test3_notInstaneof() {
        Gate gate = new Gate(1);
        Task gateCopy = new Task(TaskType.WAIT);
        boolean test = gate.equals(gateCopy);
        assertFalse(test);
    }

    @Test
    public void equals_Test4_null() {
        Gate gate = new Gate(1);
        Task gateCopy = null;
        boolean test = gate.equals(gateCopy);
        assertFalse(test);
    }

    @Test
    public void hashcode_Test1_equal() {
        Gate gate1 = new Gate(3);
        Gate gate2 = new Gate(3);
        int testHash = gate1.hashCode();
        int testHash2 = gate2.hashCode();
        assertEquals(testHash, testHash2);
    }

    @Test
    public void hashcode_Test2_notEqual() {
        Gate gate1 = new Gate(3);
        Gate gate2 = new Gate(4);
        int testHash = gate1.hashCode();
        int testHash2 = gate2.hashCode();
        assertNotEquals(testHash, testHash2);
    }

    @Test
    public void encode_Test1_AircraftAtGate() {
        Gate gate = new Gate(3);
        try {
            gate.parkAircraft(aircraft1);
        } catch (NoSpaceException e) {
            e.printStackTrace();
        }
        String testString = gate.encode();
        assertEquals("3:ABC123", testString);
    }

    @Test
    public void encode_Test2_NoAircraftAtGate() {
        Gate gate = new Gate(3);
        String testString = gate.encode();
        assertEquals("3:empty", testString);
    }
}
